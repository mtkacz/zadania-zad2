# -*- encoding: utf-8 -*-

import email.utils
import sys
import smtplib

from email.mime.text import MIMEText

def create_and_send_email():
    # Odczyt danych od użytkownika
    print("Email nadawcy:")
    from_addr=sys.stdin.readline().strip()
    print("Podaj adresata:")
    to_addrs=sys.stdin.readline().strip()
    print("Podaj temat wiadomości:")
    subject=sys.stdin.readline().strip()
    print("Napisz treść wiadomości kończąc ją <CRLF>.<CRLF>:")
    message_body=""
    while True:
        line=sys.stdin.readline()
        if line==".\n":
            break    
        message_body=message_body+line

    # Stworzenie wiadomości
    msg = MIMEText(message_body, _charset='utf-8')
    msg['From']=email.utils.formataddr((from_addr,from_addr))
    msg['To']=email.utils.formataddr((to_addrs,to_addrs))
    msg['Subject']=subject
    
    try:
        # Połączenie z serwerem pocztowym
        server=smtplib.SMTP('194.29.175.240', 25)

        # Ustawienie parametrów
        server.set_debuglevel(True)
        server.starttls()
        # Autentykacja
        print("Podaj hasło dla "+from_addr.split('@')[0]+": ")
        passwd=sys.stdin.readline().strip()
        server.login(from_addr.split('@')[0], passwd)
        # Wysłanie wiadomości
        server.sendmail(from_addr, [to_addrs, ], msg.as_string())
        pass

    finally:
        # Zamknięcie połączenia
        server.close()
        pass

decision = 't'

while decision in ['t', 'T', 'y', 'Y']:
    create_and_send_email()
    decision = raw_input('Wysłać jeszcze jeden list? ')
